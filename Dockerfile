from node:18

RUN npm i -g pnpm
WORKDIR /app
COPY package.json .
COPY pnpm-lock.yaml .
COPY tsconfig.base.json .

RUN pnpm install --frozen-lockfile

ARG SERVICE
ENV SERVICE $SERVICE

COPY apps/ ./apps/
COPY libs/ ./libs/

RUN pnpm run nx build $SERVICE --configuration=production

CMD ["pnpm","run","start:prod","$SERVICE"]
