import { MessagePatterns, NATS_CLIENT } from '@app/constants';
import { GetUserInput, UserDto } from '@app/nats-dto';
import { GreetUserInput } from '@app/nats-dto/greeting.dto';
import { Controller, Get, Inject, Param, ParseIntPipe } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Controller()
export class GatewayController {
  constructor(@Inject(NATS_CLIENT) private readonly natsClient: ClientProxy) {}

  @Get('/')
  getUsers(): Observable<UserDto[]> {
    return this.natsClient.send<UserDto[]>(MessagePatterns.GetUsers, {});
  }

  @Get('/:id')
  getUser(@Param('id', ParseIntPipe) userId: number): Observable<UserDto> {
    return this.natsClient.send<UserDto, GetUserInput>(
      MessagePatterns.GetUser,
      {
        id: userId,
      },
    );
  }

  @Get('/:id/hello')
  getHello(@Param('id', ParseIntPipe) userId: number): Observable<string> {
    return this.natsClient.send<string, GreetUserInput>(
      MessagePatterns.GreetUser,
      { userId },
    );
  }
}
