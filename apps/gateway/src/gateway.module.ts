import { NATS_CLIENT } from '@app/constants';
import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { GatewayController } from './gateway.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: NATS_CLIENT,
        transport: Transport.NATS,
        options: {
          servers: [`nats://${process.env.NATS_HOST}:${process.env.NATS_PORT}`],
        },
      },
    ]),
  ],
  controllers: [GatewayController],
  providers: [],
})
export class GatewayModule {}
