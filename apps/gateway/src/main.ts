import { NestFactory } from '@nestjs/core';
import { NatsOptions, Transport } from '@nestjs/microservices';
import { GatewayModule } from './gateway.module';

async function bootstrap() {
  const app = await NestFactory.create(GatewayModule);
  app.connectMicroservice<NatsOptions>({
    transport: Transport.NATS,
    options: {
      servers: [`nats://${process.env.NATS_HOST}:${process.env.NATS_PORT}`],
      reconnect: true,
      reconnectTimeWait: 5000,
      maxReconnectAttempts: 5,
      waitOnFirstConnect: true,
      queue: 'gateway',
    },
  });
  await app.listen(3000);
}
bootstrap();
