import { GetUserInput, UserDto } from '@app/nats-dto';
import { Injectable } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { MessagePatterns } from '@app/constants';
import { UsersService } from './users.service';

@Injectable()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @MessagePattern(MessagePatterns.GetUsers)
  getUsers(): UserDto[] {
    return this.usersService.getUsers();
  }

  @MessagePattern(MessagePatterns.GetUser)
  getUser(@Payload() { id }: GetUserInput): UserDto {
    return this.usersService.getUser(id);
  }
}
