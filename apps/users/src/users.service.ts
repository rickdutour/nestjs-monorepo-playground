import { UserDto } from '@app/nats-dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  private readonly users: UserDto[] = [
    { id: 1, name: 'Felix' },
    { id: 2, name: 'Jane' },
    { id: 3, name: 'Bob' },
  ];

  getUsers(): UserDto[] {
    return this.users;
  }

  getUser(userId: number) {
    const user = this.users.find(({ id }) => id === userId);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  }
}
