import { NestFactory } from '@nestjs/core';
import { NatsOptions, Transport } from '@nestjs/microservices';
import { UsersModule } from './users.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<NatsOptions>(UsersModule, {
    transport: Transport.NATS,
    options: {
      servers: [`nats://${process.env.NATS_HOST}:${process.env.NATS_PORT}`],
      maxReconnectAttempts: -5,
      waitOnFirstConnect: true,
      queue: 'users',
    },
  });
  await app.listen();
}
bootstrap();
