import { MessagePatterns, NATS_CLIENT } from '@app/constants';
import { GetUserInput, UserDto } from '@app/nats-dto';
import { GreetUserInput } from '@app/nats-dto/greeting.dto';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';
import { GreetingService } from './greeting.service';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class GreetingController {
  constructor(
    private readonly greetingService: GreetingService,
    @Inject(NATS_CLIENT) private readonly natsClient: ClientProxy,
  ) {}

  @MessagePattern(MessagePatterns.GreetUser)
  async getHello(@Payload() { userId }: GreetUserInput): Promise<string> {
    const user = await firstValueFrom(
      this.natsClient.send<UserDto, GetUserInput>(MessagePatterns.GetUser, {
        id: userId,
      }),
    );

    return this.greetingService.getHello(user.name);
  }
}
