import { NestFactory } from '@nestjs/core';
import { NatsOptions, Transport } from '@nestjs/microservices';
import { GreetingModule } from './greeting.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<NatsOptions>(
    GreetingModule,
    {
      transport: Transport.NATS,
      options: {
        servers: [`nats://${process.env.NATS_HOST}:${process.env.NATS_PORT}`],
        maxReconnectAttempts: -5,
        waitOnFirstConnect: true,
        queue: 'greeting',
      },
    },
  );
  await app.init();
  await app.listen();
}
bootstrap();
