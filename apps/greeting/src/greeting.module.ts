import { NATS_CLIENT } from '@app/constants';
import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { GreetingController } from './greeting.controller';
import { GreetingService } from './greeting.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: NATS_CLIENT,
        transport: Transport.NATS,
        options: {
          servers: [`nats://${process.env.NATS_HOST}:${process.env.NATS_PORT}`],
        },
      },
    ]),
  ],
  controllers: [GreetingController],
  providers: [GreetingService],
})
export class GreetingModule {}
