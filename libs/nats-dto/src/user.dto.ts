export class UserDto {
  id: number;
  name: string;
}

export class GetUserInput {
  id: number;
}
