export enum MessagePatterns {
  GetUser = 'users.get',
  GetUsers = 'users.list',
  GreetUser = 'users.greet',
}

export const NATS_CLIENT = 'NATS_CLIENT';
