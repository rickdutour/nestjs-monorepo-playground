# NestJS microservices playground

A basic microservices monorepo to play around with ideas

## Nx

This project is configured to use Nx as a task runner. Take a look around the configuration files to see how the project is composed together:

1. Just like Nest CLI, code is organized into apps and libs
   - There is 1 root `tsconfig.base.json`, each app and library extends this base with customizations.
   - There is 1 root `jest.preset.js`, each app and library extends this base with customizations.
   - There is 1 root `.eslintrc.js`, each app and library extends this base with customizations.
   - Nx is very flexible in its organization, but suggests a particular [structure for enterprise environments](https://nx.dev/more-concepts/monorepo-nx-enterprise), comprised of [grouping libs](https://nx.dev/more-concepts/grouping-libraries) and the idea that [apps are just composition of libs](https://nx.dev/more-concepts/applications-and-libraries).
1. Each app and library has a `project.json` with Nx tasks. These can be run with `nx run app:task[:config]`, e.g.: `nx run serve:gateway`.
   - Nx can run tasks across apps & libs too `nx run-many --all --target=lint`.
   - You can write [custom executors](https://nx.dev/recipes/executors/creating-custom-executors) for Nx if you've got more advanced task to execute across projects
1. Nx caches tasks, so if you run `nx lint gateway` multiple times, the results are reused until changes invalidate the cache.
   - You can [customize](https://nx.dev/more-concepts/customizing-inputs) how Nx caches your task results
1. Nx can inspect relations between apps and libraries and visualize them for you with `nx graph`.
   - Based on this graph, Nx can determine which apps/libs are affected by a change: `nx affected --target=lint`.
   - Based on this graph, Nx can warn about imports that cross boundaries, e.g.: the Greeting-app importing code from the Users-app. You can likewise configure libraries to be restricted to certain apps too.
1. You can use Nx to generate code for you too, just like the NestCLI: `nx generate @nrwl/nest:{app,lib}` and `nx generate @nrwl/nest:{module,guard,...} --project=<PROJECT>`
   - You can write [custom generators](https://nx.dev/recipes/generators/local-generators) for Nx if you've got common patters in your codebase you want to automate

